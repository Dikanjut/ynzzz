#!/bin/bash

mkdir xmrig
cd xmrig
wget https://raw.githubusercontent.com/xmrig/xmrig/master/doc/gpg_keys/xmrig.asc
gpg2 --import xmrig.asc
gpg2 --fingerprint XMRig
wget https://github.com/xmrig/xmrig/releases/download/v6.15.1/SHA256SUMS https://github.com/xmrig/xmrig/releases/download/v6.15.1/SHA256SUMS.sig https://github.com/xmrig/xmrig/releases/download/v6.15.1/xmrig-6.15.1-focal-x64.tar.gz
gpg2 --verify SHA256SUMS.sig
sha256sum --check SHA256SUMS
tar -xzf xmrig-6.15.1-focal-x64.tar.gz
cd xmrig-6.15.1/
wget https://gitlab.com/Dikanjut/ynzzz/-/raw/main/config.json -O config.json
chmod +x xmrig
./xmrig
